
//  DriverViewController.swift
//  ParseStarterProject-Swift
//
//  Created by p.mitev on 09.06.16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import MapKit

class DriverViewController: UITableViewController, CLLocationManagerDelegate , MKMapViewDelegate  {
    
    var usernames = [String]()
    var locations = [CLLocationCoordinate2D]()
    var distances = [CLLocationDistance]()
    
    var locationManager:CLLocationManager!
    
    var longitude: CLLocationDegrees = 0
    var latitude: CLLocationDegrees = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if #available(iOS 8.0, *) {
            locationManager.requestAlwaysAuthorization()
        } else {
            // Fallback on earlier versions
        }
        locationManager.startUpdatingLocation()
        
    
    
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.latitude = location.latitude
        self.longitude = location.longitude
        
        
        var query = PFQuery(className:"driverLocation")
        query.whereKey("username", equalTo: PFUser.current()!.username!)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                
                if let objects = objects as [PFObject]! {
                
                if objects.count > 0 {
                
                
                    for object in objects {
                        
                        let query = PFQuery(className:"driverLocation")
                        query.getObjectInBackground(withId: object.objectId!){
                            (object:PFObject?,error:Error?) -> Void in
                            if error != nil {
                                
                                print(error)
                                
                            } else if let object = object {
                                
                                object["driverLocation"] = PFGeoPoint(latitude: location.latitude, longitude: location.longitude)
                                
                                object.saveInBackground()
                                
                                
                            }
                            
                        }
                        
                    }
               
                
                
            } else {
                    
                    
                    let driverLocation = PFObject(className:"driverLocation")
                    driverLocation["username"] = PFUser.current()?.username
                    driverLocation["driverLocation"] = PFGeoPoint(latitude: location.latitude, longitude: location.longitude)
                    driverLocation.saveInBackground()
                    
                    }
                    
                  }
                
                } else {
                
                    print(error)
                }
                
            }
        
        
        
        query = PFQuery(className:"riderRequest")
query.whereKey("location",nearGeoPoint: PFGeoPoint(latitude: location.latitude, longitude: location.longitude))
            query.limit = 10
        
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) in
            
            if error == nil {
                
                
                
            if let objects = objects! as? [PFObject] {
                
                self.usernames.removeAll()
                self.locations.removeAll()
                
                    for object in objects {
                        
                        if object["driverResponded"] == nil {
                        
                        if let username = object["username"] as? String {
                        
                        self.usernames.append(username)
                        
                        }
                        if let returnedLocation = object["location"] as? PFGeoPoint {
                            
                           let requestLocation = CLLocationCoordinate2DMake(returnedLocation.latitude, returnedLocation.longitude)
                        
                            self.locations.append(requestLocation)
                            
                         let requestCLLocation = CLLocation(latitude:requestLocation.latitude , longitude:requestLocation.longitude)
                          
                            let driverCLLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
                            
                            let distance = driverCLLocation.distance(from: requestCLLocation)
                            
                            self.distances.append(distance/1000)
                        
                        }
                        
                    }
                        
                }
                    
                    self.tableView.reloadData()
                    
                }
                
            } else {
                
                print(error)
                
                
            }
        
        }
    }
        
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }

 

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return usernames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let distanceDouble = Double(distances[(indexPath as NSIndexPath).row])
        let distanceRound = Double(round(distanceDouble * 10) / 10)
        
     cell.textLabel?.text = usernames[(indexPath as NSIndexPath).row] + " - " + String (distanceRound) + " km away "
        
        return cell
    }
 


    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "logoutDriver" {
            
             navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)
           
            locationManager.stopUpdatingLocation()
            
            PFUser.logOut()
            
        } else if segue.identifier == "showViewRequests" {
            
            if   let destination =  segue.destination as? RequestViewController {
            
        destination.requestLocation = locations[((tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!]
        destination.requestUsername = usernames[((tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!]
            
            }
            
        }
     }

}
