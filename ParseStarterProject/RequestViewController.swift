//
//  RequestViewController.swift
//  ParseStarterProject-Swift
//
//  Created by p.mitev on 11.06.16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import MapKit


class RequestViewController: UIViewController ,CLLocationManagerDelegate , MKMapViewDelegate  {
    @IBOutlet var map: MKMapView!

    
    var requestLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(0, 0)
    var requestUsername:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(requestUsername)
        print(requestLocation)
        
        let region = MKCoordinateRegion(center: requestLocation, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.map.setRegion(region, animated: true)
   
       
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = requestLocation
        objectAnnotation.title = requestUsername
        self.map.addAnnotation(objectAnnotation)
    }
    
    
    @IBAction func pickUpRider(_ sender: AnyObject) {
        
        let query = PFQuery(className:"riderRequest")
        query.whereKey("username", equalTo: requestUsername)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                
                
                
                if let objects = objects {
                    for object in objects {
                        
                        let query = PFQuery(className:"riderRequest")
                        query.getObjectInBackground(withId: object.objectId!){
                            (object:PFObject?,error:Error?) -> Void in
                            if error != nil {
                                
                                print(error)
                                
                            } else if let object = object {
                                
                                object["driverResponded"] = PFUser.current()?.username!
                                
                                object.saveInBackground()
                               
                                 let requestCLLocation = CLLocation(latitude:self.requestLocation.latitude , longitude:self.requestLocation.longitude)
                                
                              CLGeocoder().reverseGeocodeLocation(requestCLLocation, completionHandler: { (placemarks, error) in
                                
                                if error != nil {
                                 
                                    print(error!)
                                
                                } else {
                                    
                                    if placemarks!.count > 0 {
                                     
                                        let pm = placemarks![0] 
                                        
                                        let mkPm = MKPlacemark(placemark: pm)
                                        
                                        let mapItem = MKMapItem(placemark: mkPm)
                                        
                                        mapItem.name = self.requestUsername
                                        
                                        
                                        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                                        
                                        mapItem.openInMaps(launchOptions: launchOptions)

                                    } else  {
                                        
                                        print("Problem with data recieved from geocoder")
                                        
                                    }
                             }
                        
                                
                    })
                          
                                
                                
                                
                            }
                            
                        }
                        
                    }
                    
                } else {
                    
                    print(error)
                }
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
