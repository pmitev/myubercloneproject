//
//  RiderViewController.swift
//  ParseStarterProject-Swift
//
//  Created by p.mitev on 06.06.16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import MapKit

class RiderViewController: UIViewController ,CLLocationManagerDelegate , MKMapViewDelegate {

    @IBOutlet var callUberButton: UIButton!
 
    
    @IBOutlet var map: MKMapView!
   
     var riderRequestActive = false
    var driverOnTheWay = false
    
    var locationManager:CLLocationManager!
    
    var longitude: CLLocationDegrees = 0
    var latitude: CLLocationDegrees = 0
    
    @IBAction func callUber(_ sender: AnyObject) {
        
        if riderRequestActive == false {
            
            
                
                      
        let riderRequest = PFObject(className:"riderRequest")
        riderRequest["username"] = PFUser.current()?.username
        riderRequest["location"] = PFGeoPoint(latitude: latitude, longitude: longitude)
      
        riderRequest.saveInBackground {
            (success: Bool, error: Error?) -> Void in
            if (success) {
               
               self.callUberButton.setTitle("Cancel Uber", for: UIControlState())
                
                
        
                
            } else {
                
                 if #available(iOS 8.0, *) {
                    
                let alert = UIAlertController(title: "Could not call Uber", message: "Please try again!",preferredStyle:UIAlertControllerStyle.alert)
               
                    alert.addAction(UIAlertAction(title: "Ok",style: UIAlertActionStyle.default ,handler:  nil))
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
                
            }
        }
 
            riderRequestActive = true
            
        } else {
            
            
            
            self.callUberButton.setTitle("Call An Uber", for: UIControlState())
            
            riderRequestActive = false
    
    let query = PFQuery(className:"riderRequest")
    query.whereKey("username", equalTo: (PFUser.current()?.username)!)
    query.findObjectsInBackground {
    (objects: [PFObject]?, error: Error?) -> Void in
    
    if error == nil {
    

    
    if let objects = objects {
    for object in objects {
    object.deleteInBackground()
        
        
        }
        
      }
        
    } else {
    
       print(error)
         }
        

       
      }
   }
}

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       locationManager = CLLocationManager()
       
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if #available(iOS 8.0, *) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            // Fallback on earlier versions
        }
        locationManager.startUpdatingLocation()
        
            }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.latitude = location.latitude
        self.longitude = location.longitude
        
        let query = PFQuery(className:"riderRequest")
      
        
        if PFUser.current()!.username != nil {
            

       query.whereKey("username", equalTo: PFUser.current()!.username!)
            
            
        
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                
                
                
                if let objects = objects! as? [PFObject] {
                    
                    
                    
                    for object in objects {
                        
                        if  let driverUsername = object["driverResponded"] {
                        
                  
                            
                            
                            
                            let query = PFQuery(className:"driverLocation")
                            query.whereKey("username", equalTo: driverUsername)
                            query.findObjectsInBackground {
                                (objects: [PFObject]?, error: Error?) -> Void in
                                
                                if error == nil {
                                    
                                    
                                    
                                    if let objects = objects! as? [PFObject] {
                                        
                                        
                                        
                                        for object in objects {
                                            
                                           if let driverLocation = object["driverLocation"] as? PFGeoPoint  {
                                            
                                            print(driverLocation)
                                            
                                            let driverCLLocation = CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                                            
                                            let userCLLocation = CLLocation(latitude: location.latitude, longitude:location.longitude)
                                            
                                            let distanceInMeters = userCLLocation.distance(from: driverCLLocation)
                                            
                                            let distanceInPreferedMeasurement = distanceInMeters/1000
                                            let roundedTwoDigitsDistance = Double(round(10*distanceInPreferedMeasurement)/10)
                                            
                                            print(roundedTwoDigitsDistance)
                               self.callUberButton.setTitle("Driver is \(roundedTwoDigitsDistance)km away!", for: UIControlState())
                                            
                                   
                                   self.driverOnTheWay = true
                                            
                                            let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                                            let lonDelta = abs(driverLocation.latitude - location.longitude) * 2 - 0.005
                                            
                                             let latDelta = abs(driverLocation.longitude - location.latitude) * 2 - 0.005
                                            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta))
                                            
                                            self.map.setRegion(region, animated: true)
                                            
                                            self.map.removeAnnotations(self.map.annotations)
                                            var pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.latitude, location.longitude)
                                            var objectAnnotation = MKPointAnnotation()
                                            objectAnnotation.coordinate = pinLocation
                                            objectAnnotation.title = "Your Location"
                                            self.map.addAnnotation(objectAnnotation)
                                            
                                            
                                            pinLocation  = CLLocationCoordinate2DMake(driverLocation.latitude, driverLocation.longitude)
                                            objectAnnotation = MKPointAnnotation()
                                            objectAnnotation.coordinate = pinLocation
                                            objectAnnotation.title = "Driver Location"
                                            self.map.addAnnotation(objectAnnotation)
                                            
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
 
        
      // print("locations = \(location.latitude) \(location.longitude)")
        
        if driverOnTheWay == false {
        
        let center = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.map.setRegion(region, animated: true)
        
        self.map.removeAnnotations(map.annotations)
        let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = pinLocation
        objectAnnotation.title = "Your Location"
        self.map.addAnnotation(objectAnnotation)
       
    }
    
}
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        
        if segue.identifier == "logoutRider" {
            
              locationManager.stopUpdatingLocation()
        
        PFUser.logOut()
    
        }
    }
   
}
