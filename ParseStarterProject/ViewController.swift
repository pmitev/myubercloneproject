/**
* Copyright (c) 2015-present, Parse, LLC.
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/

import UIKit
import Parse
import LocalAuthentication

class ViewController: UIViewController,UITextFieldDelegate {

    
    func displayAlert(_ title: String, message: String) {
        
            
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                
                // Fallback on earlier versions
            }
        }
    
    var signUpState = true
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var `switch`: UISwitch!
    @IBOutlet var driverLabel: UILabel!
    
    @IBOutlet var riderLabel: UILabel!
    @IBAction func signUp(_ sender: AnyObject) {
        
        if username.text == "" || password.text == "" {
        
        displayAlert("Missing Field(s)", message: "Username and Password are required")
       
        } else {
        
            let user = PFUser()
            user.username = username.text
            user.password = password.text
            
            if signUpState == true {
            
            user["isDriver"] = `switch`.isOn
            
            user.signUpInBackground {
                (succeeded: Bool, error: Error?) -> Void in
                if let error = error {
                    let errorString = (error as NSError).userInfo ["error"] as? String
                    
                   self.displayAlert("Sign Up failed!", message: errorString!)
                } else {
                    
                    if  self.`switch`.isOn == true  {
                        
                        
                    self.performSegue(withIdentifier: "loginDriver", sender: self)
                        
                    } else {
                        
                    self.performSegue(withIdentifier: "loginRider", sender: self)
                        
                
                    
                    }
                
                }
        }
                
            } else {
                
    PFUser.logInWithUsername(inBackground: username.text!, password:password.text!) {
                    (user: PFUser?, error: Error?) -> Void in
                    if  let user = user {
                     
                        if  user["isDriver"]! as! Bool == true  {
                            
                            
                            self.performSegue(withIdentifier: "loginDriver", sender: self)
                            
                        } else {
                            
                            self.performSegue(withIdentifier: "loginRider", sender: self)
                            
                            
                            
                        }

                        
                    } else {
                        
                        let errorString = (error! as NSError).userInfo["error"] as? String
                        
                        self.displayAlert("Login failed!", message: errorString!)

                    }
                }
          }
    }
        
}
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func toggleSignUp(_ sender: AnyObject) {
        
        if signUpState == true {
        
        signUpButton.setTitle("Log In", for: UIControlState())
            toggleSignUpButton.setTitle("Switch to Sign Up", for: UIControlState())
            
            signUpState = false
            driverLabel.alpha = 0
            riderLabel.alpha = 0
            `switch`.alpha = 0
            
        } else {
            
        
                
                signUpButton.setTitle("Sign Up", for: UIControlState())
                toggleSignUpButton.setTitle("Switch to Login", for: UIControlState())
                
                signUpState = true
                driverLabel.alpha = 1
                riderLabel.alpha = 1
                `switch`.alpha = 1
            
            
            
        }
        
    
    }
    
    @IBOutlet var toggleSignUpButton: UIButton!
    @IBOutlet var signUpButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        
        var error:NSError?
        
        if #available(iOS 8.0, *) {
            
            let authenticationContext = LAContext()
            
            if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
                
            {
                authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please log in with TouchID") { (success, error) in
                    
                    if success {
                        
                        //User authenticated
                        
                    } else  {
                        
                        if let error = error {
                            
                            //there was an error
                        } else {
                            
                            // user did not authenticated
                            
                        }
                    }
                }
                
            } else {
                
                //no touch Id
            }
        } else {
            // Fallback on earlier versions
        }
        self.username.delegate = self;
           self.password.delegate = self;
 
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    

        
    }

   
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if PFUser.current()?.username != nil {
            
            if  PFUser.current()?["isDriver"]! as! Bool == true  {
                
                
                self.performSegue(withIdentifier: "loginDriver", sender: self)
                
            } else {
                
                self.performSegue(withIdentifier: "loginRider", sender: self)
                
                
                
            }

            
    }
  }
}
