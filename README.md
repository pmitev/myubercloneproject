Uber Clone App, with Login and Signup options for the Driver and the Rider.The users could be located on the map.With AplleMaps the Driver can navigate to pick up the Rider.

################################################################################

![pickUpRider.png](https://bitbucket.org/repo/8BG9Ko/images/3826087305-pickUpRider.png)

![logIn.PNG](https://bitbucket.org/repo/8BG9Ko/images/3215299185-logIn.PNG)

![requestDriver.png](https://bitbucket.org/repo/8BG9Ko/images/2976012486-requestDriver.png)

![riderList.png](https://bitbucket.org/repo/8BG9Ko/images/542240771-riderList.png)

![routeToRider.png](https://bitbucket.org/repo/8BG9Ko/images/3864340978-routeToRider.png)